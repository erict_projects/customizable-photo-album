<!DOCTYPE html>
<?php
	if(!isset($_SESSION['login'])){
		session_start();
	}
	if(!$_SESSION['login']){
		header("Location: index.php");
	}elseif ($_SESSION['user'] != 'riceant'){
		header("Location: photoAlbum.php");
	}
?>
<html>
<head>
    <title>New Photo Entry</title>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <meta name="keywords" content="photo, gallery" />
    <link rel="stylesheet" type="text/css" href="index.css" />
    <script type="text/javascript" src="index.js"></script>
	<script src="jquery-1.9.1.min.js"></script>
</head>

<body>

<div class="logo" align='center'>
	<img src="images/photoArrange_logo.png" width = "400px" height="100px"/>
</div>
<div id='searchBox'>
	<form method='post' action='search.php'>
	<input name = 'searchQuery' type='text'>
	<input class = 'button_long' type='submit' value='Search Photos'>
	</form>
</div>
<div class="menu">
	<div class="menu_item">
		<a href="logout.php">Logout</a>
	</div>
	<div class="menu_item">
		<a href="photoAlbum.php">Photo Albums</a><br>
	</div>
	<div class="menu_item">
		<a href="newPhoto.php">Add Photo</a><br>
	</div>
	<div class="menu_item">
		<a href="newAlbum.php">Add Album</a><br>
	</div>
	<div class="menu_item">
		<a href="editStuff.php">Edit Stuff</a><br>
	</div>
	<div class="menu_item">
		<a href="change_password.php">Account</a><br>
	</div>
</div>

<div id="error_msg" align='center'>
</div>

<div class="table" align='center'>
	<table class='text' width='600px' border='1'>
	<form method='post' action='newPhoto.php' enctype='multipart/form-data'>
		<tr height='30px'>
		<td width='200px' colspan='2' align='center'><b>Create New Photo Entry</b></td>
		</tr>
		<tr height='15px'>
		<td colspan='2'>* = required</td>
		</tr>
		<tr height='30px'>
		<td>*Upload New Photo:</td>
		<td><input type='file' name='newPhoto' /></td>
		</tr>
		<tr height='30px'>
		<td>Photo Title:</td>
		<td><input id='photoName' class='inputbox' name='photoName' type='text'></td>
		</tr>
		<tr height='80px'>
		<td>Caption:</td>
		<td><textarea name='caption' rows='4' cols='22'></textarea></td>
		</tr>
		<tr height='30px'>
		<td>Date Taken:</td>
		<td><input id='photoDate' class='dateBox' name='photoDate' type='date'></td>
		</tr>
		<tr height='80px'>
		<td>*Add to Album(s): <br>(Multi-select using ctrl+click)</td>
		<td>
			<select class='selectBox' name='albums[]' multiple>
			<?php
				$fp = fopen("sql_account.txt", "r");
				while(!feof($fp)) {
					$login_info = explode(' ', fgets($fp));
				}
				$mysqli = new mysqli($login_info[0], $login_info[1], $login_info[2], $login_info[3]);
				fclose($fp);
				$result = $mysqli->query("SELECT albumID, albumName FROM Albums ORDER BY orderNum");
				$counter = 0;
				$table = array(array());
				while ($array = $result->fetch_row()) {
					$table[$counter] = $array;
					$counter++;
					if (isset($array[0])){
						echo "<option value='".$array[0]."'>".$array[1]."</option>";
					}
				}
				$mysqli->close();
			?>
			</select>
		</td>
		</tr>
		<tr height='30px'>
		<td></td>
		<td><input class='button_medium' type='submit' name='submit' value='Add Photo'></td>
		</tr>
	</form>
	</table>
</div>
<?php
if(isset($_POST['submit'])){
if(isset($_FILES['newPhoto']) && isset($_POST['albums'])){

	if ($_FILES['newPhoto']['error'] == 0) {
		$file = $_FILES['newPhoto']['name'];
		if(preg_match("/(.jpg)|(.JPG)|(.jpeg)|(.JPEG)|(.png)|(.PNG)|(.gif)|(.GIF)|(.bmp)|(.BMP)$/", $file)){
			$photoFile = "images/album/".$file;
			
			if(isset($_POST['photoName'])){
				$photoName = $_POST['photoName'];
			} else {
				$photoName = "";
			}
			if(isset($_POST['caption'])){
				$caption = $_POST['caption'];
			} else {
				$caption = "";
			}
			if(isset($_POST['photoDate'])){
				$dateTaken = $_POST['photoDate'];
			} else {
				$dateTaken = "";
			}
			if(isset($_POST['albums'])){
				$albums = $_POST['albums'];
			} 		
			
			$fp = fopen("sql_account.txt", "r");
			while(!feof($fp)) {
				$login_info = explode(' ', fgets($fp));
			}
			$mysqli = new mysqli($login_info[0], $login_info[1], $login_info[2], $login_info[3]);
			fclose($fp);
			
			$result = $mysqli->query("SELECT * FROM Photos WHERE photoFile='".$photoFile."'");
			if(!($temp=$result->fetch_row())){
				$result = $mysqli->query("SELECT photoID FROM Photos ORDER BY photoID DESC LIMIT 1");
				
				$temp = $result->fetch_row();
				$photoID = $temp[0] + 1;
				
				$mysqli->query("INSERT INTO Photos VALUES('".$photoID."', '".$photoFile."', '".$photoName."', '".$caption."', '".$dateTaken."')");
				foreach($albums as $albumID){
					$result3= $mysqli->query("SELECT albumOrder FROM PhotosInAlbums ORDER BY albumOrder DESC LIMIT 1");
					$temp = $result3->fetch_row();
					$nextOrder = $temp[0] + 1;
					$mysqli->query("INSERT INTO PhotosInAlbums VALUES('".$photoID."', '".$albumID."', '".$nextOrder."')");
					$mysqli->query("UPDATE Albums SET dateModified = NOW() WHERE albumID = ".$albumID);
				}
				move_uploaded_file($_FILES['newPhoto']['tmp_name'],	"images/album/".$file);
				
				echo '<script type="text/javascript">
				$("#error_msg").css("color", "DarkGreen");
				$("#error_msg").html("Photo uploaded successfully!");
				</script>';
			} else{
				echo '<script type="text/javascript">
				$("#error_msg").css("color", "red");
				$("#error_msg").html("Photo file already exists. Please upload a different photo or change the file name.");
				</script>';
			}
		} else{
			echo '<script type="text/javascript">
			$("#error_msg").css("color", "red");
			$("#error_msg").html("Invalid photo format. File extension must be among .jpg, .jpeg, .png, .gif, .bmp");
			</script>';
		}
	} else{
		echo '<script type="text/javascript">
		$("#error_msg").css("color", "red");
		$("#error_msg").html("Invalid input. <br>Make sure a valid photo file is chosen and an existing album selected.");
		</script>';
	}
}else{
	echo '<script type="text/javascript">
	$("#error_msg").css("color", "red");
	$("#error_msg").html("Invalid input. <br>Make sure a valid photo file is chosen and an existing album selected.");
	</script>';
}
}

?>
</body>
</html>