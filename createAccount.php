<!DOCTYPE html>
<?php
	if(!isset($_SESSION['login'])){
		session_start();
	}
	if(isset($_SESSION['login']) && $_SESSION['login']){
		header("Location: photoAlbum.php");
	}
?>
<html>
<head>
    <title>Create New Account</title>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <meta name="keywords" content="photo, gallery" />
    <link rel="stylesheet" type="text/css" href="index.css" />
    <script type="text/javascript" src="index.js"></script>
	<script src="jquery-1.9.1.min.js"></script>
</head>

<body>

<div class="logo" align='center'>
<img src="images/photoArrange_logo.png" width = "400px" height="100px"/>
</div>
<div class="menu2">
	<div class="menu_item">
		<a href="index.php">Login</a>
	</div>
</div>

<div class="login_form">
	<div id="error_msg">
	</div>
	<div class="subtitle">
	Create Account
	</div>
	<div>
		<form method='post' action='createAccount.php'>
		Username:<br>
		<input class='inputbox' name='username' type='text'><br>
		<br>
		Password:<br>
		<input class='inputbox' name='password' type='password'><br>
		<br>
		<input class = 'button' type='submit' value='Create'><br>
		<br>
		</form>
	</div>
</div>

<?php
	
	if (isset($_POST['username'])){
		$fp = fopen("sql_account.txt", "r");
		while(!feof($fp)) {
			$login_info = explode(' ', fgets($fp));
		}
		
		$username = $_POST['username'];
		$password = $_POST['password'];
		
		$mysqli = new mysqli($login_info[0], $login_info[1], $login_info[2], $login_info[3]);
		fclose($fp);
		$result = $mysqli->query("SELECT * FROM Accounts WHERE username = '" . $username . "'");
		$table = array();
		while ($array = $result->fetch_row()) {
			$table = $array;
		}
		if (isset($table[1])){
			echo '<script type="text/javascript">
			$("#error_msg").css("color", "red");
			$("#error_msg").text("Account already exists.");
			</script>';
		} elseif($username != "") {
			$mysqli->query("INSERT INTO Accounts VALUES('" .$username. "', '" .hash('sha256', $password). "')");
			echo '<script type="text/javascript">
			$("#error_msg").css("color", "DarkGreen");
			$("#error_msg").text("Account created.");
			</script>';
		}
		$mysqli->close();
	}
	
?>
</body>
</html>