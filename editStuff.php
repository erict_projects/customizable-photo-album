<!DOCTYPE html>
<?php
	if(!isset($_SESSION['login'])){
		session_start();
	}
	if(!$_SESSION['login']){
		header("Location: index.php");
	}elseif ($_SESSION['user'] != 'riceant'){
		header("Location: photoAlbum.php");
	}
?>
<html>
<head>
    <title>Edit Stuff</title>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <meta name="keywords" content="photo, gallery" />
    <link rel="stylesheet" type="text/css" href="index.css" />
    <script type="text/javascript" src="index.js"></script>
	<script src="jquery-1.9.1.min.js"></script>
</head>

<body>
<div class="logo" align='center'>
	<img src="images/photoArrange_logo.png" width = "400px" height="100px"/>
</div>
<div id='searchBox'>
	<form method='post' action='search.php'>
	<input name = 'searchQuery' type='text'>
	<input class = 'button_long' type='submit' value='Search Photos'>
	</form>
</div>
<div class="menu">
	<div class="menu_item">
		<a href="logout.php">Logout</a>
	</div>
	<div class="menu_item">
		<a href="photoAlbum.php">Photo Albums</a><br>
	</div>
	<div class="menu_item">
		<a href="newPhoto.php">Add Photo</a><br>
	</div>
	<div class="menu_item">
		<a href="newAlbum.php">Add Album</a><br>
	</div>
	<div class="menu_item">
		<a href="editStuff.php">Edit Stuff</a><br>
	</div>
	<div class="menu_item">
		<a href="change_password.php">Account</a><br>
	</div>
</div>
<div id='error_msg' align='center'>
</div>

<div class="table" align='center'>
<br>
	<table class='text3' width='1000px' border='1'>
		<tr height= '30'>
		<td width='300px'>Photo Album Name/Description</td>
		<td width='400px'>Photo Entries
		<?php
		if(isset($_POST["albumID"])){
			$fp = fopen("sql_account.txt", "r");
			while(!feof($fp)) {
				$login_info = explode(' ', fgets($fp));
			}
			$mysqli = new mysqli($login_info[0], $login_info[1], $login_info[2], $login_info[3]);
			fclose($fp);
			$result = $mysqli->query("SELECT albumName FROM Albums WHERE albumID=".$_POST["albumID"]);
			$temp=$result->fetch_row();
			echo " in ".$temp[0];
		}
		?>
		</td>
		<td width='300px'>Edit</td>
		</tr>
		<tr>
		<td valign='top'>
			<?php
				function printAlbum($title, $description, $albumID){
					echo "<table class='border_bottom' width='300px'>
					<tr height ='70' >
					<td width='70px'>".$title."</td>
					<td width='110px'>".$description."</td>
					<td width='120px'>
					<span id='forms_one_line'>
					<form method='post' action='editStuff.php' onsubmit='return confirm(\"Are you sure you want to delete the entire album?\")'>
					<input name = 'albumID' type='hidden' value='".$albumID."'>
					<input class = 'button' name='delete' type='submit' value='Delete'>
					</form>
					</span>
					<span id='forms_one_line2'>
					<form method='post' action='editStuff.php'>
					<input name = 'albumID' type='hidden' value='".$albumID."'>
					<input class = 'button_tiny' name='up' type='submit' value='Up'>
					</form>
					</span>
					<form method='post' action='editStuff.php'>
					<input name = 'albumID' type='hidden' value='".$albumID."'>
					<input class = 'button_small' name='edit' type='submit' value='Edit'>
					<input class = 'button_small' name='down' type='submit' value='Down'>
					</form>
					</td>
					</tr>
					</table>";
				}
				if(isset($_POST["up"])){
					$fp = fopen("sql_account.txt", "r");
					while(!feof($fp)) {
						$login_info = explode(' ', fgets($fp));
					}
					$mysqli = new mysqli($login_info[0], $login_info[1], $login_info[2], $login_info[3]);
					fclose($fp);
					$result = $mysqli->query("SELECT orderNum FROM Albums WHERE albumID=".$_POST["albumID"]);
					$temp= $result->fetch_row();
					$swap1 = $temp[0];
					$result = $mysqli->query("SELECT orderNum FROM Albums WHERE orderNum <". $swap1." ORDER BY orderNum DESC LIMIT 1");
					if($temp= $result->fetch_row()){
						$swap2 = $temp[0];
						$mysqli->query("UPDATE Albums SET orderNum=".$swap1." WHERE orderNum=".$swap2);
						$mysqli->query("UPDATE Albums SET orderNum=".$swap2." WHERE albumID=".$_POST["albumID"]);
						echo '<script type="text/javascript">
						$("#error_msg").css("color", "DarkGreen");
						$("#error_msg").html("<br>Order changed successfully!");
						</script>';
					}
					
					$mysqli->close();
				}
				if(isset($_POST["upP"])){
					$fp = fopen("sql_account.txt", "r");
					while(!feof($fp)) {
						$login_info = explode(' ', fgets($fp));
					}
					$mysqli = new mysqli($login_info[0], $login_info[1], $login_info[2], $login_info[3]);
					fclose($fp);
					$result = $mysqli->query("SELECT albumOrder FROM PhotosInAlbums WHERE photoID=".$_POST["photoID"]." 
						AND albumID=".$_POST["albumID"]);
					$temp= $result->fetch_row();
					$swap1 = $temp[0];
					$result = $mysqli->query("SELECT albumOrder FROM PhotosInAlbums WHERE albumOrder <". $swap1." 
						AND albumID=".$_POST["albumID"]." ORDER BY albumOrder DESC LIMIT 1");
					if($temp= $result->fetch_row()){
						$swap2 = $temp[0];
						$mysqli->query("UPDATE PhotosInAlbums SET albumOrder=".$swap1." WHERE albumOrder=".$swap2);
						$mysqli->query("UPDATE PhotosInAlbums SET albumOrder=".$swap2." WHERE photoID=".$_POST["photoID"]);
						echo '<script type="text/javascript">
						$("#error_msg").css("color", "DarkGreen");
						$("#error_msg").html("<br>Order changed successfully!");
						</script>';
					}
					
					$mysqli->close();
				}
				if(isset($_POST["down"])){
					$fp = fopen("sql_account.txt", "r");
					while(!feof($fp)) {
						$login_info = explode(' ', fgets($fp));
					}
					$mysqli = new mysqli($login_info[0], $login_info[1], $login_info[2], $login_info[3]);
					fclose($fp);
					$result = $mysqli->query("SELECT orderNum FROM Albums WHERE albumID=".$_POST["albumID"]);
					$temp= $result->fetch_row();
					$swap1 = $temp[0];
					$result = $mysqli->query("SELECT orderNum FROM Albums WHERE orderNum >". $swap1." ORDER BY orderNum LIMIT 1");
					if($temp= $result->fetch_row()){
						$swap2 = $temp[0];
						$mysqli->query("UPDATE Albums SET orderNum=".$swap1." WHERE orderNum=".$swap2);
						$mysqli->query("UPDATE Albums SET orderNum=".$swap2." WHERE albumID=".$_POST["albumID"]);
						echo '<script type="text/javascript">
						$("#error_msg").css("color", "DarkGreen");
						$("#error_msg").html("<br>Order changed successfully!");
						</script>';
					}
					$mysqli->close();
				}
				if(isset($_POST["downP"])){
					$fp = fopen("sql_account.txt", "r");
					while(!feof($fp)) {
						$login_info = explode(' ', fgets($fp));
					}
					$mysqli = new mysqli($login_info[0], $login_info[1], $login_info[2], $login_info[3]);
					fclose($fp);
					$result = $mysqli->query("SELECT albumOrder FROM PhotosInAlbums WHERE photoID=".$_POST["photoID"]." 
						AND albumID=".$_POST["albumID"]);
					$temp= $result->fetch_row();
					$swap1 = $temp[0];
					$result = $mysqli->query("SELECT albumOrder FROM PhotosInAlbums WHERE albumOrder >". $swap1." 
						AND albumID=".$_POST["albumID"]." ORDER BY albumOrder LIMIT 1");
					if($temp= $result->fetch_row()){
						$swap2 = $temp[0];
						$mysqli->query("UPDATE PhotosInAlbums SET albumOrder=".$swap1." WHERE albumOrder=".$swap2);
						$mysqli->query("UPDATE PhotosInAlbums SET albumOrder=".$swap2." WHERE photoID=".$_POST["photoID"]);
						echo '<script type="text/javascript">
						$("#error_msg").css("color", "DarkGreen");
						$("#error_msg").html("<br>Order changed successfully!");
						</script>';
					}
					
					$mysqli->close();
				}
				
				if(isset($_POST["delete"])){
					$fp = fopen("sql_account.txt", "r");
					while(!feof($fp)) {
						$login_info = explode(' ', fgets($fp));
					}
					$mysqli = new mysqli($login_info[0], $login_info[1], $login_info[2], $login_info[3]);
					fclose($fp);
					$result = $mysqli->query("SELECT photoID, photoFile FROM Photos WHERE photoID IN (SELECT photoID FROM PhotosInAlbums WHERE albumID=".$_POST['albumID'].")"); 
					$counter = 0;
					$delete = array(array());
					while ($array = $result->fetch_row()) {
						$delete[$counter] = $array;
						$counter++;
						$result = $mysqli->query("SELECT photoID FROM PhotosInAlbums WHERE photoID=".$array[0]." GROUP BY photoID HAVING COUNT(albumID)>1");
						if(!($array2 = $result->fetch_row())){
							unlink($array[1]);
							$mysqli->query("DELETE FROM Photos WHERE photoID =".$array[0]); 
						}
					}
					
					$mysqli->query("DELETE FROM Albums WHERE albumID=". $_POST['albumID']); 
					$mysqli->query("DELETE FROM PhotosInAlbums WHERE albumID=". $_POST['albumID']); 
					echo '<script type="text/javascript">
					$("#error_msg").css("color", "DarkGreen");
					$("#error_msg").html("<br>Album deleted successfully!");
					</script>';
					$mysqli->close();
				}
				
				if(isset($_POST["changeData"]) && isset($_POST["edit"]) && isset($_POST["photoID"]) && isset($_POST['albums'])){
					if(isset($_POST['photoName'])){
						$photoName = $_POST['photoName'];
					} else {
						$photoName = "";
					}
					if(isset($_POST['caption'])){
						$caption = $_POST['caption'];
					} else {
						$caption = "";
					}
					if(isset($_POST['photoDate'])){
						$dateTaken = $_POST['photoDate'];
					} else {
						$dateTaken = "";
					}
					if(isset($_POST['albums'])){
						$albums = $_POST['albums'];
					} 		
					
					$fp = fopen("sql_account.txt", "r");
					while(!feof($fp)) {
						$login_info = explode(' ', fgets($fp));
					}
					$mysqli = new mysqli($login_info[0], $login_info[1], $login_info[2], $login_info[3]);
					fclose($fp);
					$photoID = $_POST["photoID"];
					
					$mysqli->query("UPDATE Photos SET photoName='".$photoName."', 
						caption='".$caption."', dateTaken='".$dateTaken."' WHERE photoID=". $photoID);
						
					$result5= $mysqli->query("SELECT * FROM PhotosInAlbums WHERE photoID=".$photoID);
					$existing = array(array());
					$counter=0;
					while ($array = $result5->fetch_row()) {
						$existing[$counter] = $array;
						$counter++;
					}
					
					$mysqli->query("DELETE FROM PhotosInAlbums WHERE photoID=".$photoID);
					foreach($albums as $albumID){
						$oldOrder = false;
						$nextOrder= 0;
						foreach($existing as $entry){
							if($photoID == $entry[0] && $albumID==$entry[1]){
								$oldOrder=true;
								$nextOrder=$entry[2];
								break;
							}
						}
						if(!$oldOrder){
							$result3= $mysqli->query("SELECT albumOrder FROM PhotosInAlbums ORDER BY albumOrder DESC LIMIT 1");
							$temp = $result3->fetch_row();
							$nextOrder = $temp[0] + 1;
						}
						$mysqli->query("INSERT INTO PhotosInAlbums VALUES('".$photoID."', '".$albumID."', '".$nextOrder."')");
						$mysqli->query("UPDATE Albums SET dateModified = NOW() WHERE albumID = ".$albumID);
					}
					
					
					echo '<script type="text/javascript">
					$("#error_msg").css("color", "DarkGreen");
					$("#error_msg").html("<br>Photo information changed successfully!");
					</script>';
					$mysqli->close();
				}
				
				$fp = fopen("sql_account.txt", "r");
				while(!feof($fp)) {
					$login_info = explode(' ', fgets($fp));
				}
				$mysqli = new mysqli($login_info[0], $login_info[1], $login_info[2], $login_info[3]);
				fclose($fp);
				$result = $mysqli->query("SELECT * FROM Albums ORDER BY orderNum");
				$counter = 0;
				$table = array(array());
				while ($array = $result->fetch_row()) {
					$table[$counter] = $array;
					$counter++;
					if (isset($array[3])){
						printAlbum($array[1],$array[4],$array[0]);
					}
				}
				$mysqli->close();
			?>
		</td>
		
		<td valign='top' align='center'>
			<?php
				function printPhoto($name, $caption, $file, $photoID, $albumID){
					echo "<table class='border_bottom' width='400px'>
					<tr height ='70px' >
					<td width='70px'>".$name."</td>
					<td width='110px'>".$caption."</td>
					<td width='100px'><img src='" . $file . "' width='100' height='70'/></td>
					<td width='120px'>
					<form method='post' action='editStuff.php'>
					<input name = 'albumID' type='hidden' value='".$albumID."'>
					<input name = 'photoID' type='hidden' value='".$photoID."'>
					<input class = 'button' name='deleteP' type='submit' value='Delete'>
					<input class = 'button_tiny' name='upP' type='submit' value='Up'>
					<br>
					<input class = 'button_small' name='edit' type='submit' value='Edit'>
					<input class = 'button_small' name='downP' type='submit' value='Down'>
					</form>
					</td>
					</tr>
					</table>";
				}
				if(isset($_POST["deleteP"])){
					$fp = fopen("sql_account.txt", "r");
					while(!feof($fp)) {
						$login_info = explode(' ', fgets($fp));
					}
					$mysqli = new mysqli($login_info[0], $login_info[1], $login_info[2], $login_info[3]);
					fclose($fp);
					$result = $mysqli->query("SELECT photoFile FROM Photos WHERE photoID=". $_POST['photoID']);
					$delete = $result->fetch_row();
					if (isset($delete[0])){
						unlink($delete[0]);
					}
					$mysqli->query("DELETE FROM Photos WHERE photoID=". $_POST['photoID']); 
					$mysqli->query("DELETE FROM PhotosInAlbums WHERE photoID=". $_POST['photoID']);
					echo '<script type="text/javascript">
					$("#error_msg").css("color", "DarkGreen");
					$("#error_msg").html("<br>Photo deleted successfully!");
					</script>';					
				}
				if(isset($_POST["albumID"])){
					$fp = fopen("sql_account.txt", "r");
					while(!feof($fp)) {
						$login_info = explode(' ', fgets($fp));
					}
					$mysqli = new mysqli($login_info[0], $login_info[1], $login_info[2], $login_info[3]);
					fclose($fp);
					$result = $mysqli->query("SELECT photoID, photoFile, photoName, caption 
						FROM Photos NATURAL JOIN PhotosInAlbums NATURAL JOIN Albums WHERE albumID =".$_POST['albumID']." ORDER BY albumOrder");
					$counter = 0;
					$table = array(array());
					while ($array = $result->fetch_row()) {
						$table[$counter] = $array;
						$counter++;
						if (isset($array[1])){
							printPhoto($array[2],$array[3],$array[1], $array[0], $_POST['albumID']);
						}
					}
					if ($counter == 0){
						echo "<br>This album is empty";
					}
					$mysqli->close();
				}
				
			?>
		</td>
		
		
		<td valign='top'>
			<table class='text2' width='300px'>
			<form method='post' action='editStuff.php'>
				<tr height='220px'>
				<td id='photoFile' colspan='2'><img src='images/no_image.jpg' width='280' height='220'/></td>
				</tr>
				<tr height='15px'>
				<td colspan='2'>* = required</td>
				</tr>
				<tr height='30px'>
				<td width='100px'>Photo Title:</td>
				<td width='200px'><input id='photoName' class='inputbox_Edit' name='photoName' type='text'></td>
				</tr>
				<tr height='80px'>
				<td>Caption:</td>
				<td><textarea id='captionText' name='caption' rows='4' cols='20'></textarea></td>
				</tr>
				<tr height='30px'>
				<td>Date Taken:</td>
				<td><input id='photoDate' class='dateBox' name='photoDate' type='date'></td>
				</tr>
				<tr height='80px'>
				<td>*Add to Album(s): <br>(Multi-select using ctrl+click)</td>
				<td>
					<select id='selectBox' class='selectBox_Edit' name='albums[]' multiple>
					<?php
						$fp = fopen("sql_account.txt", "r");
						while(!feof($fp)) {
							$login_info = explode(' ', fgets($fp));
						}
						$mysqli = new mysqli($login_info[0], $login_info[1], $login_info[2], $login_info[3]);
						fclose($fp);
						
						$selected = array(array());
						if(isset($_POST["edit"]) && isset($_POST["photoID"])){
							$counter2 = 0;
							$result2 = $mysqli->query("SELECT * FROM PhotosInAlbums WHERE photoID =" . $_POST["photoID"]);
							while ($array2 = $result2->fetch_row()) {
								$selected[$counter2] = $array2;
								$counter2++;
							}
						}
						$result = $mysqli->query("SELECT albumID, albumName FROM Albums");
						$counter = 0;
						$table = array(array());
						while ($array = $result->fetch_row()) {
							$table[$counter] = $array;
							$counter++;
							if (isset($array[0])){
								$selectThis = false;
								if (isset($_POST["photoID"])){
									foreach($selected as $entry){
										if(isset($entry[1]) && $entry[1] == $array[0]){
											$selectThis = true;
											break;
										}
									}
								}
								if($selectThis){
									echo "<option value='".$array[0]."' selected>".$array[1]."</option>";
								}else{
									echo "<option value='".$array[0]."'>".$array[1]."</option>";
								}
							}
						}
						$mysqli->close();
						
						if(isset($_POST["edit"]) && isset($_POST["photoID"])){
							$fp = fopen("sql_account.txt", "r");
							while(!feof($fp)) {
								$login_info = explode(' ', fgets($fp));
							}
							$mysqli = new mysqli($login_info[0], $login_info[1], $login_info[2], $login_info[3]);
							fclose($fp);
							$result = $mysqli->query("SELECT * FROM Photos WHERE photoID =" . $_POST["photoID"]);
							$photo = $result->fetch_row();
							
							echo "<script type='text/javascript'>
							$('#photoName').val('".$photo[2]."');
							$('#captionText').html('".$photo[3]."');
							$('#photoDate').val('".$photo[4]."');
							$('#photoFile').html('<img src=\"".$photo[1]."\" width=\"280\" height=\"220\"/>');
							</script>";
							$mysqli->close();
						}
						
					?>
					</select>
				</td>
				</tr>
				<tr height='30px'>
				<td></td>
				<td>
				<?php
					if(isset($_POST["edit"]) && isset($_POST["photoID"]) && isset($_POST["albumID"])){
						echo "
						<input name = 'albumID' type='hidden' value='".$_POST["albumID"]."'>
						<input name = 'photoID' type='hidden' value='".$_POST["photoID"]."'>
						<input name = 'edit' type='hidden' value='thisValueDoesNothing'>
						";
					}
				?>
				<input class='button_medium' type='submit' name='changeData' value='Save Changes'></td>
				</tr>
			</form>
			</table>
		</td>
		</tr>
	</table>
	<br>
	<br>
</div>

</body>
</html>