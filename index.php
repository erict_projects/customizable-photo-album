<!DOCTYPE html>
<?php
	session_start(); // At top of page
	if(isset($_SESSION['login'])){
		if($_SESSION['login'])
		{
		header("Location: photoAlbum.php"); // redirects
		}
	}
?>

<html>
<head>
    <title>Photo Arrange</title>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <meta name="keywords" content="photo, gallery" />
    <link rel="stylesheet" type="text/css" href="index.css" />
	<LINK REL="SHORTCUT ICON" HREF="../images/favicon.ico">
    <script type="text/javascript" src="index.js"></script>
	<script src="jquery-1.9.1.min.js"></script>
</head>

<body>

<div class="logo" align='center'>
<img src="images/photoArrange_logo.png" width = "400px" height="100px"/>
</div>

<div class="login_form">
	<div id="error_msg">
	</div>
	<div class="subtitle">
	Login
	</div>
	<div>
		<form method='post' action='login.php'>
		Username:<br>
		<input id='username' class='inputbox' name='username' type='text'><br>
		<br>
		Password:<br>
		<input class='inputbox' name='password' type='password'><br>
		<br>
		<input type='checkbox' name='remember' value='Remember me'>Remember me<br>
		<input class = 'button' type='submit' value='Login'><br>
		<br>
		</form>
		<form method='post' action='forgot_password.php'>
		<input class = 'button_longer' type='submit' value='Forgot Password?'>
		</form>
		<form method='post' action='createAccount.php'>
		<input class = 'button_longer' type='submit' value='Create New Account'>
		</form>
	</div>
</div>

<?php
	if (isset($_SESSION['login'])){
		if (!$_SESSION['login']){
			echo '<script type="text/javascript">
			$("#error_msg").css("color", "red");
			$("#error_msg").text("Invalid login information.");
			</script>';
		}
	}
	if (isset($_COOKIE["username"])){
		echo '<script type="text/javascript">
		$("#username").val("'.$_COOKIE["username"].'");
		</script>';
	}
	
?>
</body>
</html>