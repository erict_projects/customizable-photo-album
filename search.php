<!DOCTYPE html>
<?php
	if(!isset($_SESSION['login'])){
		session_start();
	}
	if(!$_SESSION['login']){
		header("Location: index.php");
	}
?>
<html>
<head>
    <title>Search Results</title>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <meta name="keywords" content="photo, gallery" />
    <link rel="stylesheet" type="text/css" href="index.css" />
    <script type="text/javascript" src="index.js"></script>
	<script src="jquery-1.9.1.min.js"></script>
</head>

<body>
<div class="logo" align='center'>
	<img src="images/photoArrange_logo.png" width = "400px" height="100px"/>
</div>
<div id='searchBox'>
	<form method='post' action='search.php'>
	<input name = 'searchQuery' type='text'>
	<input class = 'button_long' type='submit' value='Search Photos'>
	</form>
</div>
<div class="menu">
	<div class="menu_item">
		<a href="logout.php">Logout</a>
	</div>
	<div class="menu_item">
		<a href="photoAlbum.php">Photo Albums</a><br>
	</div>
	<?php
		if($_SESSION['user']=='riceant'){
	echo '<div class="menu_item">
		<a href="newPhoto.php">Add Photo</a><br>
	</div>
	<div class="menu_item">
		<a href="newAlbum.php">Add Album</a><br>
	</div>
	<div class="menu_item">
		<a href="editStuff.php">Edit Stuff</a><br>
	</div>';}
	?>
	<div class="menu_item">
		<a href="change_password.php">Account</a><br>
	</div>
</div>
<div id='error_msg' align='center'>
</div>
<div class="table" align='center'>
	<br>
	<span class='albumText_dark'>Search Results</span>
	<table class='text' width='800' border='1'>

		<tr>
		<td width='150'>Photo Title</td>
		<td width='200'>Caption</td>
		<td width='100'>Date Taken</td>
		<td width='350'>Photo</td>
		</tr>
		<?php
			function printPhoto($title, $caption, $taken, $photo){
				echo "<tr height ='250'>
				<td>".$title."</td>
				<td>".$caption."</td>
				<td>".$taken."</td>
				<td><img src='" . $photo . "' width='350' height='250'/></td>
				</tr>";
			}

			if(isset($_POST['searchQuery'])){
				$search = $_POST['searchQuery'];
			} else{
				$search = "";
			}
			$arrSearch = explode(' ', $search);
			$photoCount=0;
			$photoID_list=array();
			
			$fp = fopen("sql_account.txt", "r");
			while(!feof($fp)) {
				$login_info = explode(' ', fgets($fp));
			}
			$mysqli = new mysqli($login_info[0], $login_info[1], $login_info[2], $login_info[3]);
			fclose($fp);
			foreach($arrSearch as $word){
				$result = $mysqli->query("SELECT photoName, caption, dateTaken, photoFile, photoID FROM Photos WHERE 
					caption LIKE '%".$word."%' OR photoName LIKE '%".$word."%' LIMIT 30");
				$counter = 0;
				$table = array(array());
				while ($array = $result->fetch_row()) {
					$table[$counter] = $array;
					$counter++;
					if (isset($array[3])){
						$is_duplicate = false;
						foreach($photoID_list as $existing){
							if ($array[4] == $existing){
								$is_duplicate = true;
								break;
							}
						}
						if(!$is_duplicate){
							printPhoto($array[0],$array[1],$array[2],$array[3]);
							array_push($photoID_list, $array[4]);
							$photoCount++;
						}
					}
				}
				
			}
			if ($photoCount>29){
				echo '<script type="text/javascript">
				$("#error_msg").css("color", "black");
				$("#error_msg").html("<br>Some results have been omitted because search results are limited to 30 photos.");
				</script>';
			}
			if($photoCount == 0){
				echo '<script type="text/javascript">
				$("#error_msg").css("color", "black");
				$("#error_msg").html("<br>No photos found to match search query.");
				</script>';
			}

			$mysqli->close();
		?>
	</table>
	<br>
	<br>
</div>
</body>
</html>