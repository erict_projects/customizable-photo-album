<!DOCTYPE html>
<?php
	if(!isset($_SESSION['login'])){
		session_start();
	}
	if(!$_SESSION['login']){
		header("Location: index.php");
	}
?>
<html>
<head>
    <title>Photo Album</title>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <meta name="keywords" content="photo, gallery" />
    <link rel="stylesheet" type="text/css" href="index.css" />
    <script type="text/javascript" src="index.js"></script>
	<script src="jquery-1.9.1.min.js"></script>
</head>

<body>
<div class="logo" align='center'>
	<img src="images/photoArrange_logo.png" width = "400px" height="100px"/>
</div>
<div id='searchBox'>
	<form method='post' action='search.php'>
	<input name = 'searchQuery' type='text'>
	<input class = 'button_long' type='submit' value='Search Photos'>
	</form>
</div>
<div class="menu">
	<div class="menu_item">
		<a href="logout.php">Logout</a>
	</div>
	<div class="menu_item">
		<a href="photoAlbum.php">Photo Albums</a><br>
	</div>
	<?php
		if($_SESSION['user']=='riceant'){
	echo '<div class="menu_item">
		<a href="newPhoto.php">Add Photo</a><br>
	</div>
	<div class="menu_item">
		<a href="newAlbum.php">Add Album</a><br>
	</div>
	<div class="menu_item">
		<a href="editStuff.php">Edit Stuff</a><br>
	</div>';}
	?>
	<div class="menu_item">
		<a href="change_password.php">Account</a><br>
	</div>
</div>

<div class="table" align='center'>
	<table class='text' width='800' border='1'>
		<tr>
		<td width='150'>Photo Album Name</td>
		<td width='200'>Description</td>
		<td width='100'>Date Created</td>
		<td width='150'>Last Modified</td>
		<td width='100'>Sample Photo</td>
		<td width='100'>Link</td>
		</tr>
		<?php
			function printAlbum($title, $description, $created, $modified, $photo, $albumID){
				echo "<tr height ='70'>
				<td>".$title."</td>
				<td>".$description."</td>
				<td>".$created."</td>
				<td>".$modified."</td>
				<td><img src='" . $photo . "' width='100' height='70'/></td>
				<td><form method='post' action='albumPhotos.php'>
				<input name = 'albumID' type='hidden' value='".$albumID."'>
				<input class = 'button' type='submit' value='View >>'>
				</form>
				</td>
				</tr>";
			}

			$fp = fopen("sql_account.txt", "r");
			while(!feof($fp)) {
				$login_info = explode(' ', fgets($fp));
			}
			$mysqli = new mysqli($login_info[0], $login_info[1], $login_info[2], $login_info[3]);
			fclose($fp);
			$result = $mysqli->query("SELECT * FROM Albums ORDER BY orderNum");
			$counter = 0;
			$table = array(array());
			while ($array = $result->fetch_row()) {
				$table[$counter] = $array;
				$counter++;
				if (isset($array[3])){
					$result2 = $mysqli->query("SELECT photoFile FROM Photos NATURAL JOIN PhotosInAlbums 
						NATURAL JOIN Albums WHERE albumID = " . $array[0] . " ORDER BY albumOrder LIMIT 1");
					$image = $result2->fetch_row();
					if (isset($image[0])){
						printAlbum($array[1],$array[4],$array[2],$array[3], $image[0], $array[0]);
					} else{
						printAlbum($array[1],$array[4],$array[2],$array[3], 'images/no_image.jpg', $array[0]);
					}
				}
			}
			$mysqli->close();
		?>
	</table>
	<br>
	<br>
</div>
</body>
</html>