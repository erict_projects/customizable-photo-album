<!DOCTYPE html>
<?php
	if(!isset($_SESSION['login'])){
		session_start();
	}
	if(!$_SESSION['login']){
		header("Location: index.php");
	}elseif ($_SESSION['user'] != 'riceant'){
		header("Location: photoAlbum.php");
	}
?>
<html>
<head>
    <title>Create New Album</title>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <meta name="keywords" content="photo, gallery" />
    <link rel="stylesheet" type="text/css" href="index.css" />
    <script type="text/javascript" src="index.js"></script>
	<script src="jquery-1.9.1.min.js"></script>
</head>

<body>
<div class="logo" align='center'>
	<img src="images/photoArrange_logo.png" width = "400px" height="100px"/>
</div>
<div id='searchBox'>
	<form method='post' action='search.php'>
	<input name = 'searchQuery' type='text'>
	<input class = 'button_long' type='submit' value='Search Photos'>
	</form>
</div>
<div class="menu">
	<div class="menu_item">
		<a href="logout.php">Logout</a>
	</div>
	<div class="menu_item">
		<a href="photoAlbum.php">Photo Albums</a><br>
	</div>
	<div class="menu_item">
		<a href="newPhoto.php">Add Photo</a><br>
	</div>
	<div class="menu_item">
		<a href="newAlbum.php">Add Album</a><br>
	</div>
	<div class="menu_item">
		<a href="editStuff.php">Edit Stuff</a><br>
	</div>
	<div class="menu_item">
		<a href="change_password.php">Account</a><br>
	</div>
</div>

<div id="error_msg" align='center'>
</div>

<div class="table" align='center'>
	<table class='text' width='600px' border='1'>
	<form method='post' action='newAlbum.php'>
		<tr height='30px'>
		<td width='200px' colspan='2' align='center'><b>Create New Photo Album</b></td>
		</tr>
		<tr height='15px'>
		<td colspan='2'>* = required</td>
		</tr>
		<tr height='30px'>
		<td>*Album Name:</td>
		<td><input id='albumName' class='inputbox' name='albumName' type='text'></td>
		</tr>
		<tr height='80px'>
		<td>Description:</td>
		<td><textarea name='message' rows='4' cols='22'></textarea></td>
		</tr>
		<tr height='30px'>
		<td></td>
		<td><input class='button_longer' name='submit' type='submit' value='Create Album'></td>
		</tr>
	</form>
	</table>
</div>
<?php
if(isset($_POST['submit'])){
if(isset($_POST['albumName']) && strlen($_POST['albumName']) > 0){
	$albumName = $_POST['albumName'];	

	//replace special chars
	$albumName = str_replace("\\", "\\\\", $albumName);
	$albumName = str_replace("'", "\'", $albumName);
	$albumName = str_replace('"', '\"', $albumName);
	$albumName = str_replace("%", "\%", $albumName);
	$albumName = str_replace("_", "\_", $albumName);
	$albumName = str_replace("%", "\%", $albumName);
	//$charset = needs to validate input
	
	if(isset($_POST['message'])){
		$description = $_POST['message'];
	} else {
		$description = "";
	}

	$fp = fopen("sql_account.txt", "r");
	while(!feof($fp)) {
		$login_info = explode(' ', fgets($fp));
	}
	$mysqli = new mysqli($login_info[0], $login_info[1], $login_info[2], $login_info[3]);
	fclose($fp);
	$result = $mysqli->query("SELECT * FROM Albums WHERE albumName='".$albumName."'");
	if(!($temp=$result->fetch_row())){
	
		$result = $mysqli->query("SELECT albumID FROM Albums ORDER BY albumID DESC LIMIT 1");
		
		$temp = $result->fetch_row();
		$albumID = $temp[0] + 1;

		$result2= $mysqli->query("SELECT orderNum FROM Albums ORDER BY orderNum DESC LIMIT 1");
		$temp = $result2->fetch_row();
		$nextOrder = $temp[0] + 1;
		$mysqli->query("INSERT INTO Albums VALUES('".$albumID."', '".$albumName."', CURDATE(), NOW(), '".$description."', '".$nextOrder."')");
		
		echo '<script type="text/javascript">
		$("#error_msg").css("color", "DarkGreen");
		$("#error_msg").html("Album created successfully!");
		</script>';
	} else{
		echo '<script type="text/javascript">
		$("#error_msg").css("color", "red");
		$("#error_msg").html("Album already exists.");
		</script>';
	}
}else{
	echo '<script type="text/javascript">
	$("#error_msg").css("color", "red");
	$("#error_msg").html("Please give your album a name.");
	</script>';
}
}

?>
</body>
</html>