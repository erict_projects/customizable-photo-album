<!DOCTYPE html>

<?php
	if(!isset($_SESSION['login'])){
		session_start();
	}
	if(!$_SESSION['login']){
		header("Location: index.php");
	}
?>
<html>
<head>
    <title>Change Password</title>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <meta name="keywords" content="photo, gallery" />
    <link rel="stylesheet" type="text/css" href="index.css" />
    <script type="text/javascript" src="index.js"></script>
	<script src="jquery-1.9.1.min.js"></script>
</head>

<body>
<div class="logo" align='center'>
<img src="images/photoArrange_logo.png" width = "400px" height="100px"/>
</div>
<div id='searchBox'>
	<form method='post' action='search.php'>
	<input name = 'searchQuery' type='text'>
	<input class = 'button_long' type='submit' value='Search Photos'>
	</form>
</div>
<div class="menu">
	<div class="menu_item">
		<a href="logout.php">Logout</a>
	</div>
	<div class="menu_item">
		<a href="photoAlbum.php">Photo Albums</a><br>
	</div>
	<?php
	if($_SESSION["user"]=="riceant"){
	echo '<div class="menu_item">
		<a href="newPhoto.php">Add Photo</a><br>
	</div>
	<div class="menu_item">
		<a href="newAlbum.php">Add Album</a><br>
	</div>
	<div class="menu_item">
		<a href="editStuff.php">Edit Stuff</a><br>
	</div>';}
	?>
	<div class="menu_item">
		<a href="change_password.php">Account</a><br>
	</div>
</div>
<div class="login_form">
	<div id="error_msg">
	</div>
	<div class="subtitle">
	Change Password
	</div>
	<div>
		<form method='post' action='change_password.php'>
		Username: <br>
		<input class='inputbox' name='username' type='text'><br>
		<br>
		Current Password: <br>
		<input class='inputbox' name='password' type='password'><br>
		<br>
		New Password:<br>
		<input class='inputbox' name='passwordNew' type='password'><br>
		<br>
		Confirm New Password:<br>
		<input class='inputbox' name='passwordNew2' type='password'><br>
		<br>
		<input class = 'button_longer' type='submit' value='Change Password'><br>
		<br>
		</form>
	</div>
</div>

<?php
	if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['passwordNew']) && isset($_POST['passwordNew2'])){
		$fp = fopen("sql_account.txt", "r");
		while(!feof($fp)) {
			$login_info = explode(' ', fgets($fp));
		}
		
		$username = $_POST['username'];
		$password = $_POST['password'];
		$passwordNew = $_POST['passwordNew'];
		$passwordNew2 = $_POST['passwordNew2'];
		
		$mysqli = new mysqli($login_info[0], $login_info[1], $login_info[2], $login_info[3]);
		fclose($fp);
		$result = $mysqli->query("SELECT * FROM Accounts WHERE username = '" . $username . "'");
		$table = array();
		while ($array = $result->fetch_row()) {
			$table = $array;
		}
		if (isset($table[1])){
			if(hash('sha256', $password) == $table[1]){
				if($passwordNew == $passwordNew2){
					$mysqli->query("UPDATE Accounts SET password = '" .hash('sha256', $passwordNew). "' WHERE username = '" . $username . "'");
					echo '<script type="text/javascript">
					$("#error_msg").css("color", "DarkGreen");
					$("#error_msg").text("Password changed successfully!");
					</script>';
				} else{
					echo '<script type="text/javascript">
					$("#error_msg").css("color", "red");
					$("#error_msg").text("[Confirm New Password] does not match [New Password].");
					</script>';
				}
			} else{
				echo '<script type="text/javascript">
				$("#error_msg").css("color", "red");
				$("#error_msg").text("Password change failed.");
				</script>';
			}
		} else {
			echo '<script type="text/javascript">
			$("#error_msg").css("color", "red");
			$("#error_msg").text("Username not found.");
			</script>';
		}

		$mysqli->close();
	}
?>
</body>
</html>