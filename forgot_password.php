<!DOCTYPE html>
<?php
	if(!isset($_SESSION['login'])){
		session_start();
	}
	if(isset($_SESSION['login']) && $_SESSION['login']){
		header("Location: photoAlbum.php");
	}
?>
<html>
<head>
    <title>Forgot Password</title>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <meta name="keywords" content="photo, gallery" />
    <link rel="stylesheet" type="text/css" href="index.css" />
    <script type="text/javascript" src="index.js"></script>
	<script src="jquery-1.9.1.min.js"></script>
</head>

<body>
<div class="logo" align='center'>
<img src="images/photoArrange_logo.png" width = "400px" height="100px"/>
</div>
<div class="menu2">
	<div class="menu_item">
		<a href="index.php">Login</a>
	</div>
</div>
<div class="login_form">
	<div id="error_msg">
	</div>
	<div class="subtitle">
	Password Retrieval
	</div>
	<div>
		<form method='post' action='forgot_password.php'>
		Username:<br>
		<input class='inputbox' name='username' type='text'><br>
		<br>
		Email:<br>
		<input class='inputbox' name='email' type='text'><br>
		<br>
		<input class = 'button_longer' type='submit' value='Send New Password'><br>
		<br>
		</form>
	</div>
</div>

<?php
	if (isset($_POST['username']) && isset($_POST['email'])){
		$fp = fopen("sql_account.txt", "r");
		while(!feof($fp)) {
			$login_info = explode(' ', fgets($fp));
		}
		
		$username = $_POST['username'];
		
		$mysqli = new mysqli($login_info[0], $login_info[1], $login_info[2], $login_info[3]);
		fclose($fp);
		$result = $mysqli->query("SELECT * FROM Accounts WHERE username = '" . $username . "'");
		$table = array();
		while ($array = $result->fetch_row()) {
			$table = $array;
		}
		if (isset($table[1])){
			$email = $_POST['email'] ;
			mail($email, 'PhotoArrange Password Change', 'Your password has been changed. Your username is: '. 
			$username . '      and your new password is: changeThisPassword', 'From:eyt4@cornell.edu');
			$mysqli->query("UPDATE Accounts SET password = '" .hash('sha256', 'changeThisPassword'). "' WHERE username = '" . $username . "'");
			echo '<script type="text/javascript">
			$("#error_msg").css("color", "DarkGreen");
			$("#error_msg").text("Email sent! Password changed!'.$username.'");
			</script>';
		} else {
			echo '<script type="text/javascript">
			$("#error_msg").css("color", "red");
			$("#error_msg").text("Username does not exist.");
			</script>';
		}

		$mysqli->close();
	}
?>
</body>
</html>