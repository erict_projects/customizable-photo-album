-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2015 at 03:25 AM
-- Server version: 5.5.32
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `eric_album`
--
CREATE DATABASE IF NOT EXISTS `eric_album` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `eric_album`;

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE IF NOT EXISTS `accounts` (
  `username` varchar(15) NOT NULL,
  `password` varchar(1000) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`username`, `password`) VALUES
('riceant', 'fec781427c49d429b857493d0fdc5bf5148c1c6d540bcb4cea98e59bdad4cf72'),
('test', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08');

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE IF NOT EXISTS `albums` (
  `albumID` int(11) NOT NULL,
  `albumName` varchar(100) NOT NULL,
  `dateCreated` date NOT NULL,
  `dateModified` date NOT NULL,
  `description` text,
  `orderNum` int(11) NOT NULL,
  PRIMARY KEY (`albumID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`albumID`, `albumName`, `dateCreated`, `dateModified`, `description`, `orderNum`) VALUES
(1, 'Pokemon', '2014-12-31', '2014-12-31', '', 1),
(2, 'Eric''s piano adventures', '2014-12-31', '2014-12-31', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `photoID` int(11) NOT NULL,
  `photoFile` varchar(200) NOT NULL,
  `photoName` varchar(100) DEFAULT NULL,
  `caption` text,
  `dateTaken` date DEFAULT NULL,
  PRIMARY KEY (`photoID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`photoID`, `photoFile`, `photoName`, `caption`, `dateTaken`) VALUES
(1, 'images/album/800px-Ash_Pikachu.png', 'Pikachu', '', '0000-00-00'),
(2, 'images/album/20020922-RoyThompsomHall4.jpg', '', 'Me performing at Roy Thomson Hall', '0000-00-00'),
(3, 'images/album/pokepiano.jpg', '', 'That squirtle is trying to be me.', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `photosinalbums`
--

CREATE TABLE IF NOT EXISTS `photosinalbums` (
  `photoID` int(11) NOT NULL,
  `albumID` int(11) NOT NULL,
  `albumOrder` int(11) NOT NULL,
  PRIMARY KEY (`photoID`,`albumID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `photosinalbums`
--

INSERT INTO `photosinalbums` (`photoID`, `albumID`, `albumOrder`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 1, 3),
(3, 2, 4);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
