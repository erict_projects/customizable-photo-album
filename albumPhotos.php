<!DOCTYPE html>
<?php
	if(!isset($_SESSION['login'])){
		session_start();
	}
	if(!$_SESSION['login']){
		header("Location: index.php");
	}
?>
<html>
<head>
    <title>Photo Album</title>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <meta name="keywords" content="photo, gallery" />
    <link rel="stylesheet" type="text/css" href="index.css" />
    <script type="text/javascript" src="index.js"></script>
	<script src="jquery-1.9.1.min.js"></script>
</head>

<body>
<div class="logo" align='center'>
	<img src="images/photoArrange_logo.png" width = "400px" height="100px"/>
</div>
<div id='searchBox'>
	<form method='post' action='search.php'>
	<input name = 'searchQuery' type='text'>
	<input class = 'button_long' type='submit' value='Search Photos'>
	</form>
</div>
<div class="menu">
	<div class="menu_item">
		<a href="logout.php">Logout</a>
	</div>
	<div class="menu_item">
		<a href="photoAlbum.php">Photo Albums</a><br>
	</div>
	<?php
	if($_SESSION["user"]=="riceant"){
	echo '<div class="menu_item">
		<a href="newPhoto.php">Add Photo</a><br>
	</div>
	<div class="menu_item">
		<a href="newAlbum.php">Add Album</a><br>
	</div>
	<div class="menu_item">
		<a href="editStuff.php">Edit Stuff</a><br>
	</div>';}
	?>
	<div class="menu_item">
		<a href="change_password.php">Account</a><br>
	</div>
	
</div>

<div id='albumName' class="table">
<br><br>
<span class='albumText_dark'>
<table width='622px' border='0'>
<tr>
<td width='100px'><a href='photoAlbum.php'><< Back</a></td>
<td align='center'>
<?php

	$fp = fopen("sql_account.txt", "r");
	while(!feof($fp)) {
		$login_info = explode(' ', fgets($fp));
	}
	$mysqli = new mysqli($login_info[0], $login_info[1], $login_info[2], $login_info[3]);
	fclose($fp);
	if (isset($_POST['albumID'])){
		$result = $mysqli->query("SELECT * FROM Albums WHERE albumID =".$_POST['albumID']);
	} else{
		$result = $mysqli->query("SELECT * FROM Albums");
	}
	$counter = 0;
	$table = array(array());
	while ($array = $result->fetch_row()) {
		$table[$counter] = $array;
		$counter++;
	}
	echo 'Photo Album Name: ';
	echo $table[0][1];
	
	$mysqli->close();
?>
</td>
<td width='100px'></td>
</tr>
</table>
</span>
</div>
<div class="table" align='center'>
	<table class='text' width='622px' border='1'>
		<tr>
		<td id='photoCount' class='albumText'></td>
		<td id='name' class='albumText' height='30px' width='400px'></td>
		<td id='dateTaken' class='albumText'></td>
		</tr>
		<tr>
		<td id='previousTD' class='albumText' width='100px'>
			<a id='previous' href='#albumName' target='_self'><span id='previousParent'></span></a>
		</td>
		<td id='photo' class='albumText' width='400px' height='300px'></td>
		<td id='nextTD' class='albumText' width='100px'>
			<a id='next' href='#albumName' target='_self'><span id='nextParent'>Next</span></a>
		</td>
		</tr>
		<tr>
		<td id='caption' class='albumText' colspan="3" height='100px'></td>
		</tr>
	</table>
</div>

<?php 
	$fp = fopen("sql_account.txt", "r");
	while(!feof($fp)) {
		$login_info = explode(' ', fgets($fp));
	}
	$mysqli = new mysqli($login_info[0], $login_info[1], $login_info[2], $login_info[3]);
	fclose($fp);
	if (isset($_POST['albumID'])){
		$result = $mysqli->query("SELECT * FROM Photos NATURAL JOIN PhotosInAlbums NATURAL JOIN Albums WHERE albumID =".$_POST['albumID']." ORDER BY albumOrder");
	} else{
		$result = $mysqli->query("SELECT * FROM Photos NATURAL JOIN PhotosInAlbums NATURAL JOIN Albums ORDER BY albumOrder");
	}
	$table = array(array());

	$counter = 0;
	while ($array = $result->fetch_row()) {
		$table[$counter] = $array;
		$counter++;
	}
	$mysqli->close();
?> 
<script type="text/javascript">
	var photoID = 0;
	var table = <?php echo json_encode($table); ?>;
	if (table.length > 1 && table[0].length > 1){
		changePhoto(table, photoID);
	
		$("#previous").click(function(){
			if (photoID > 0){
				$("#previousParent").html("Previous");
				photoID--;
			} 
			if (photoID < table.length - 1){
				$("#nextParent").html("Next");
			}
			if (photoID == 0){
				$("#previousParent").html("");
			}
			changePhoto(table, photoID);
		});
		$("#next").click(function(){
			if (photoID < table.length - 1){
				$("#nextParent").html("Next");
				photoID++;
			}
			if (photoID > 0){
				$("#previousParent").html("Previous");
			}
			if (photoID == table.length - 1){
				$("#nextParent").html("");
				
			}
			changePhoto(table, photoID);
		});
	} else if (table.length == 1 && table[0].length > 1){
		changePhoto(table, 0);
		$("#nextParent").html("");
	}else{
		$("#name").html('This album is currently empty');
		$("#photo").html("<img src='images/no_image.jpg' width='400' height='300'/>");
		$("#nextParent").html("");
	}
</script>

</body>
</html>